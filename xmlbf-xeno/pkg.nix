{ mkDerivation, base, bytestring, html-entities, QuickCheck
, quickcheck-instances, stdenv, tasty, tasty-hunit
, tasty-quickcheck, text, unordered-containers, xeno, xmlbf
}:
mkDerivation {
  pname = "xmlbf-xeno";
  version = "0.1.2";
  src = ./.;
  libraryHaskellDepends = [
    base bytestring html-entities text unordered-containers xeno xmlbf
  ];
  testHaskellDepends = [
    base bytestring QuickCheck quickcheck-instances tasty tasty-hunit
    tasty-quickcheck text unordered-containers xmlbf
  ];
  homepage = "https://gitlab.com/k0001/xmlbf";
  description = "xeno backend support for the xmlbf library";
  license = stdenv.lib.licenses.asl20;
}
