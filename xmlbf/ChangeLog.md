# 0.5

* BREAKING CHANGE. `element'` now returns `[Node]`. This makes the library
  safer. It's not possible to construct a malformed `Element` anymore using the
  names exported by the library. It is also ergonomic, since most functions with
  which you would want to use a manually constructed `Element` expect a `[Node]`
  anyway.

* BREAKING CHANGE. `text` now returns `Either String Node`.

* BREAKING CHANGE. `pText` now skips empty text nodes.

* BREAKING CHANGE. Removed `IsString Node` instance.

* BREAKING CHANGE. Use lazy `Text` inside `Text` nodes. This improves
  `Text` concatenation performance, performed internally by `Xmlbf`, and makes
  more intelligent use of memory when dealing with long texts.

* BREAKING CHANGE. Removed `pRead`. You are encouraged to use `pFail` or `mzero`
  if you want to write a failing parser.

* Added `node`.

* Added `pFail`.

* Added `text'`.

* Added `pChildren`.

* Added `pAnyElement`.

* Added `pName`.

* Added `NFData` instance for `Node`.

* `encode` doesn't render self-closing tags anymore. Instead, each element has
  its corresponding closing tag.


# 0.4.1

* Generalized type of `pRead`.


# 0.4

* BREAKING CHANGE. `pElement` now skips leading whitespace before an element.

* Fixed nested element parsing (#6)


# 0.3

* BREAKING CHANGE. Renamed `df` and `dfM` to `dfpos` and `dfposM` respectively.

* Added `dfpre` and `dfpreM`.

* Improved `Show` instance for `Node`.

* Added `element'`.


# 0.2

* `Text` constructor hidden in favor of a `text` function plus a `Text` pattern
  synonym, just like with `element` and `Element`.

* Documentation: Render `Element` pattern synonym.


# 0.1

* First version.
